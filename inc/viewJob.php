<?php
$user = 'root';
$pass = '';
$db = 'project_database';

$conn = new mysqli('localhost', $user, $pass, $db) or die("Cannot connect to database");

if($conn -> connect_errno){
  echo "Failed to connect". $conn -> connect_error;
  exit();
}

if($result = $conn -> query("SELECT * from progressreports WHERE jobid IN(SELECT jobid FROM jobs WHERE AddressID IN(SELECT AddressID from addresses where HouseNumber= '".$_GET['id']."'))")){
  if($result -> num_rows === 0){
    echo "<div class = 'alert alert-danger' role='alert'>
     There have been no progress reports yet.
  </div>";
  }else if($result -> num_rows >= 1){
    
    echo "<div class = 'container' style='padding-top:5%'> <div>
    <table class='table'>
    <thead>
    <tr>
      <th scope='col'>ReportID</th>
      <th scope='col'>Date</th>
      <th scope='col'>Progress Made</th>
      <th scope='col'>Progress target for next week</th>
    </tr>
  </thead>
  <tbody>
 ";
  
    while($row = $result -> fetch_array(MYSQLI_ASSOC)){
   
      echo   " <tr>
        <td>".$row["ReportID"]."</td>
        <td>".$row["TheDate"]."</td>
        <td>".$row["ProgressMade"]."</td>
        <td>".$row["ProgressNextTarget"]."</td>
        </tr>";
    }
    echo"
      </tr>
     </tbody>
  </table>
  </div>
  ";
    
  }
  
  $result -> free_result();
}
?>
