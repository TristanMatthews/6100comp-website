<?php
session_start();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="main.php?page=showjobs">Job Managment website</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
    if(isset($_SESSION['email']) && $_GET['page'] != "login"){
    echo '<a class="ml-auto " style="color:white" href="inc/logout.php">Log Out</a>';
    }
    ?>
  </div>
</nav>